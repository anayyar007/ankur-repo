// Databricks notebook source
val aws_access_key_id = "z"
val aws_secret_access_key = "z".replace("/", "%2F")

// COMMAND ----------

val test = spark.read.format("com.databricks.spark.csv").option("header", "true").option("mode", "DROPMALFORMED").load(s"/mnt/ankurs3/csv/*")

// COMMAND ----------

display(test)

// COMMAND ----------

import org.apache.spark.sql.types.{StructType, StructField, StringType, IntegerType};

val s = StructType(
  List(
    StructField("id",StringType,true),
    StructField("term",StringType,true),
    StructField("probability",StringType,true),
    StructField("topicId",StringType,true)
  )
)

// COMMAND ----------

import java.net.URLEncoder
val AccessKey = "AKIAJ446VHQ7XMIMQ3YA"
// URL Encode the Secret Key as that can contain "/" and other characters.
val SecretKey = URLEncoder.encode("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", "UTF-8")
val AwsBucketName = "ankur-s3"
val MountName = "/mnt/ankurs3"
dbutils.fs.mount(s"s3a://$AccessKey:$SecretKey@$AwsBucketName", s"$MountName")


// COMMAND ----------

display(dbutils.fs.ls("/mnt/ankurs3"))

// COMMAND ----------

val inputLocation = "/mnt/ankurs3/csv/*"

val df = spark
  .readStream
  .schema(s)
  .option("header", "true")
  .option("charset", "UTF8")
  .option("delimiter", ",")
  .csv(inputLocation)

// COMMAND ----------

import org.apache.spark.sql.types._
import org.apache.spark.sql.streaming.Trigger
val outputLocation = "dbfs:/mnt/ankurs3/incremental-lake"

val query = df
.writeStream
.trigger(Trigger.Once)
.format("parquet")
.option("checkpointLocation", "dbfs:/mnt/ankurs3/incremental-chk")
.outputMode("append")
.start(outputLocation)



// COMMAND ----------

val t = spark.read.parquet("/mnt/ankurs3/incremental-lake").count()

// COMMAND ----------

val inputLocation = "/mnt/ankurs3/csv/*"

val stream = spark
  .readStream
  .schema(s)
  .option("header", "true")
  .option("charset", "UTF8")
  .option("delimiter", ",")
  .csv(inputLocation)

// COMMAND ----------

val stream = spark
      .readStream
      .text("/mnt/ankurs3/csv/*")


// COMMAND ----------

stream.take(10)

// COMMAND ----------

import scala.util.Try

import spark.implicits._

    // Do a transform (this is emulating what PipelineModel does since that API is also a transform from Dataset[U] -> Dataset[V])
    val words = stream.as[String].transform { d =>
      d.flatMap { s =>
        // Throw an exception if the input line is hello world
        Try(s == "id,term,probability,topicId") 
        s split " "
      }
    }

//val results = words.filter(_.isSuccess).map(_.get)


// COMMAND ----------

import scala.util.Try 
import spark.implicits._

    // Do a transform (this is emulating what PipelineModel does since that API is also a transform from Dataset[U] -> Dataset[V])
    val words = stream.as[String].transform { d =>
      d.flatMap { s =>
        // Throw an exception if the input line is hello world
        if (s == "id,term,probability,topicId") 
             Some(s)
          else  
            "oops"
        
        s split " "
      }
    }



// COMMAND ----------


import org.apache.spark.sql._
import spark.implicits._
// Write stream to console using foreach writer
    val query = words.writeStream
      .foreach(new ForeachWriter[String] {
        override def process(value: String): Unit = println(value)

        override def close(errorOrNull: Throwable): Unit = {}

        override def open(partitionId: Long, version: Long): Boolean = true
      }).outputMode("update")
      .start()

    // Wait indefinitely for stream to end
    query.awaitTermination()

// COMMAND ----------

import scala.util.Try

val in = List("1", "2", "3", "abc")

val out1 = in.map(a => Try(a.toInt))

println(out1)

//val results = out1.filter(_.isSuccess).map(_.get)

//println(results)


// COMMAND ----------

import org.apache.spark.sql._

object KafkaOffset {
  case object Latest extends KafkaOffset {
    override val value = "latest"
  }
  case object Earliest extends KafkaOffset {
    override val value = "earliest"
  }
}
sealed trait KafkaOffset {
  val value: String
}

val kafkaServers = "10.20.1.14:9092,10.20.2.43:9092,10.20.3.54:9092,10.20.3.249:9092"

def readFromTopic(topicName: String, offset: KafkaOffset = KafkaOffset.Latest): Dataset[Row] =
  spark.readStream
    .format("kafka08")
    .option("kafka.bootstrap.servers", kafkaServers)
    .option("subscribe", topicName)
    .option("startingOffsets", offset.value)
    .load()

// COMMAND ----------

val transactions = spark
  .readStream
  .format("kafka08")
  .option("kafka.bootstrap.servers", "10.20.1.14:9092,10.20.2.43:9092,10.20.3.54:9092,10.20.3.249:9092")
  .option("subscribe", "transactions")
  .option("startingOffsets", "earliest")
//   .option("startingOffsets", "latest")
  .load()


// COMMAND ----------

import org.apache.spark.sql.types._

val tSchema = StructType(Seq.empty[StructField])
  .add("id", StringType)
  .add("user_id", StringType)
  .add("timestamp", TimestampType)
  .add("batch_id", StringType)
  .add("provider_transaction_id", StringType)
  .add("merchant_name", StringType)
  .add("normalized_merchant_name", StringType)
  .add("description", StringType)
  .add("category_name", StringType)
  .add("account_number", StringType)
  .add("amount", DoubleType)
  .add("transaction_date", TimestampType)
  .add("transaction_type", StringType)
  .add("address", StringType)
  .add("city", StringType)
  .add("state", StringType)
  .add("zip", StringType)
  .add("lat", StringType)
  .add("long", StringType)
  .add("provider", StringType)

// COMMAND ----------

import org.apache.spark.sql.functions._

val transactionStream = transactions
  .select(col("key") cast "string", from_json(col("value") cast "string", tSchema) as "transaction")
  .select("transaction.*")
  .filter(year($"transaction_date") <= year(current_date()))

// COMMAND ----------

import org.apache.spark.sql.streaming.ProcessingTime
import scala.concurrent.duration._

val query = transactionStream
  .withColumn("year", year($"transaction_date"))
  .withColumn("month", month($"transaction_date"))
  .withColumn("day", dayofmonth($"transaction_date"))
  .withColumn("timestamp_year", year($"timestamp"))
  .withColumn("timestamp_month", month($"timestamp"))
  .withColumn("timestamp_day", dayofmonth($"timestamp"))
  .repartition(6)
.writeStream
  .trigger(ProcessingTime(6.hours))
  .format("parquet")
  .partitionBy("timestamp_year", "timestamp_month", "timestamp_day")
  .option("checkpointLocation", "dbfs:/acorns_dw/checkpoints/transactions/1")
  .option("path", "dbfs:/acorns_dw/streaming/output/transactions/")
  .start()

// COMMAND ----------

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.types.MetadataBuilder

case class RedshiftConfig(host: String, dbName: String, username: String, password: String, s3TempDir: String, port: Option[Int] = None) {
  lazy val url = s"jdbc:redshift://$host:${port.getOrElse(5439)}/$dbName?user=$username&password=$password&datestyle=MDY"
}

val redshiftConfig = RedshiftConfig("redshift-production.ckzx9cywdjko.us-east-1.redshift.amazonaws.com", "redshiftdb", "redshift", "WiyEvKyAMPQzMfwovDxkdbDtmzuEonBjrifgPJm4cKQwggqT", "s3a://acorns-data/spark-databricks")

def rsSql(sql: String) = spark.read
    .format("com.databricks.spark.redshift")
    .option("url", redshiftConfig.url)
    .option("query", sql)
    .option("tempdir", redshiftConfig.s3TempDir)
    .option("forward_spark_s3_credentials", true)
    //.option("aws_iam_role", "arn:aws:iam::122918357635:role/redshift-production")
    .load()

def rsTable(tableName: String) = spark.read
    .format("com.databricks.spark.redshift")
    .option("url", redshiftConfig.url)
    .option("dbtable", tableName)
    .option("tempdir", redshiftConfig.s3TempDir)
    .option("forward_spark_s3_credentials", true)
    //.option("aws_iam_role", "arn:aws:iam::122918357635:role/redshift-production")
    .load()

def writeToRedshift[T](ds: Dataset[T], tableName: String) = ds.write
    .format("com.databricks.spark.redshift")
    .option("url", redshiftConfig.url)
    .option("dbtable", tableName)
    .option("tempdir", redshiftConfig.s3TempDir)
    .option("forward_spark_s3_credentials", true)

def redshiftType(colType: String) = new MetadataBuilder().putString("redshift_type", colType).build()

// COMMAND ----------

val input = sc.parallelize(Seq( 
(1, Array(5, 10, 20)), 
(1, Array(1, 2, 10, 20)), 
(2, Array(1, 2, 2, 2, 3)) 
)).toDF("id", "values")

val du = new DistinctUnion[Int](IntegerType) 
val result = input.groupBy("id").agg(du('values))

result.show(truncate = false)

// COMMAND ----------

